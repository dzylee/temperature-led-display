#include "15segdisp.h"

#include <time.h>


int main(void)
{
    long seconds = 0;
    long nanoseconds = 100000000; //250000000=0.25s, 120000000=0.12s
    struct timespec timeInterval = {seconds, nanoseconds};

    start15SegDisp();

    for( unsigned long long killtime = 0; killtime < 1; killtime++ )
    {
        for( int i = -20; i <= 100; i++ )
        {
            setValueToDisplay(i);
            nanosleep(&timeInterval, (struct timespec *) NULL);
        }

        for( int i = 99; i >= -19; i-- )
        {
            setValueToDisplay(i);
            nanosleep(&timeInterval, (struct timespec *) NULL);
        }
    }

    stop15SegDisp();

    return 0;
}