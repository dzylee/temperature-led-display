#ifndef _15_SEGMENT_DISPLAY_H_
#define _15_SEGMENT_DISPLAY_H_

/*
Initializes the 15-segment display; then launches a thread that updates what is
shown on the display whenever a new value is set by the setValueToDisplay()
function.
Run this function first before attempting to use the other two functions!
*/
void start15SegDisp(void);

/*
Sets a new value to show on the 15-segment display. Supply an integer
representing the value that you want to change to, as an argument to the
function.
*/
void setValueToDisplay(int value);

/*
Halts the thread that is responsible for updating what is shown on the display,
and clears the 15-segment display.
*/
void stop15SegDisp(void);

#endif