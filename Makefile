#The C cross-compiler being used:
C_XCOMPILER=arm-linux-gnueabihf-gcc

#C compiler:
C_COMPILER=gcc

#Compiler flags to use:
COMP_FLAGS=-Wall -std=c99 -D _POSIX_C_SOURCE=200809L -Werror

#Path to the NFS directory that the target device has access to:
NFS_OUTPUT_DIR=/home/${USER}/cmpt433/public/myApps/

#Prerequisite files for the 'testSegDisp_target' program:
SOURCE_FILES_FOR_15SEG_DISPLAY=15segdisp.c testSegDisp.c
OBJECT_FILES_FOR_15SEG_DISPLAY=${addsuffix .o, ${basename ${SOURCE_FILES_FOR_15SEG_DISPLAY}}}


#Default target:
testSegDisp_target: ${OBJECT_FILES_FOR_15SEG_DISPLAY}
	${C_XCOMPILER} ${COMP_FLAGS} ${OBJECT_FILES_FOR_15SEG_DISPLAY} -pthread -o testSegDisp_target
	mv -f testSegDisp_target ${NFS_OUTPUT_DIR}
	chmod 777 ${NFS_OUTPUT_DIR}testSegDisp_target

%.o: %.c
	${C_XCOMPILER} ${COMP_FLAGS} -c $< -o $@

testSegDisp:
	${C_XCOMPILER} ${COMP_FLAGS} -c testSegDisp.c -pthread -o testSegDisp.o

15segdisp:
	${C_XCOMPILER} ${COMP_FLAGS} -c 15segdisp.c -pthread -o 15segdisp.o

clean:
	rm -f 15segdisp.o
	rm -f testSegDisp.o
	rm -f ${NFS_OUTPUT_DIR}testSegDisp_target