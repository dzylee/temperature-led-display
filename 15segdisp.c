//version 3.0, Dec 7, 2016

#include "15segdisp.h"

#include <stdio.h>
#include <stdlib.h>         //for exit() function.
#include <fcntl.h>          //for file descriptors.
#include <sys/ioctl.h>      //for dealing with device special files.
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <unistd.h>         //for system-call functions.
#include <time.h>           //for nanosleep() function.
#include <stdbool.h>        //for boolean literals.
#include <pthread.h>        //for multiprocessing (i.e. POSIX threads)


#define SLOTS_PATHNAME "/sys/devices/platform/bone_capemgr/slots"
#define GPIO_EXPORT_PATHNAME "/sys/class/gpio/export"
#define LINUX_GPIO_NUMBER_FOR_LEFT_LED_DIGIT "61"
#define LINUX_GPIO_NUMBER_FOR_RIGHT_LED_DIGIT "44"
#define LINUX_GPIO_LEFT_LED_DIRECTION_PATHNAME "/sys/class/gpio/gpio61/direction"
#define LINUX_GPIO_RIGHT_LED_DIRECTION_PATHNAME "/sys/class/gpio/gpio44/direction"
#define LINUX_GPIO_LEFT_LED_VALUE_PATHNAME "/sys/class/gpio/gpio61/value"
#define LINUX_GPIO_RIGHT_LED_VALUE_PATHNAME "/sys/class/gpio/gpio44/value"
#define LED_ON "1"
#define LED_OFF "0"


#define I2C_BUS0_PATHNAME "/dev/i2c-0"
#define I2C_BUS1_PATHNAME "/dev/i2c-1"
#define I2C_BUS2_PATHNAME "/dev/i2c-2"
#define ADDRESS_OF_GPIO_EXTENDER 0x20

#define GPIO_REG_ADDRESS_FOR_DIRECTION_OF_REG_FOR_LOWER_HALF_OF_DIGITS 0x00
#define GPIO_REG_ADDRESS_FOR_DIRECTION_OF_REG_FOR_UPPER_HALF_OF_DIGITS 0x01
#define GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN 0x14
#define GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN 0x15

#define IMPOSSIBLE_CELSIUS_TEMPERATURE -5000

//##############################################################################################################################################################

static inline int writeToFile(char filePathname[], char valueToWrite[])
{
    FILE* ptr_file = fopen(filePathname, "w");
    if( ptr_file == NULL )
    {
        printf("Failed to open the file %s\n", filePathname);
        return 1;
    }

    int fileWriteStatus = fprintf(ptr_file, "%s", valueToWrite);
    if( fileWriteStatus <= 0 )
    {
        printf("Failed to write to the file %s\n", filePathname);
        fclose(ptr_file);
        return fileWriteStatus;
    }

    fclose(ptr_file);
    return 0;
}


//##############################################################################################################################################################

static inline void turnOnLeftDigit()
{
    writeToFile( LINUX_GPIO_LEFT_LED_VALUE_PATHNAME, LED_ON );
    return;
}

static inline void turnOffLeftDigit()
{
    writeToFile( LINUX_GPIO_LEFT_LED_VALUE_PATHNAME, LED_OFF );
    return;
}

static inline void turnOnRightDigit()
{
    writeToFile( LINUX_GPIO_RIGHT_LED_VALUE_PATHNAME, LED_ON );
    return;
}

static inline void turnOffRightDigit()
{
    writeToFile( LINUX_GPIO_RIGHT_LED_VALUE_PATHNAME, LED_OFF );
    return;
}


//##############################################################################################################################################################

static int init_I2C_bus(char busDevicePathname[], int deviceAddress)
{
    int i2cFileDesc = open(busDevicePathname, O_RDWR);
    if( i2cFileDesc < 0 )
    {
        printf("I2C DRV: Unable to open bus for read/write (%s)\n", busDevicePathname);
        perror("Error is:");
        exit(-1);
    }

    int result = ioctl(i2cFileDesc, I2C_SLAVE, deviceAddress);
    if( result < 0 )
    {
        perror("Unable to set I2C device to slave address.");
        exit(-1);
    }

    return i2cFileDesc;
}

static inline void write_to_I2C_reg(int i2cFileDesc, unsigned char regAddr, unsigned char value)
{
    unsigned char buffer[2];
    buffer[0] = regAddr;
    buffer[1] = value;
    int res = write(i2cFileDesc, buffer, 2);
    if( res != 2 )
    {
        perror("Unable to write i2c register");
        exit(-1);
    }
}

/*
This function is never used in this program. If it's ever needed, uncomment it.
*/
/*
static inline unsigned char read_from_I2C_reg(int i2cFileDesc, unsigned char regAddr)
{
    //To read a register, must first write the address:
    int res = write(i2cFileDesc, &regAddr, sizeof(regAddr));
    if( res != sizeof(regAddr) )
    {
        perror("Unable to write i2c register.");
        exit(-1);
    }

    //Now read the value and return it:
    char value = 0;
    res = read(i2cFileDesc, &value, sizeof(value));
    if( res != sizeof(value) )
    {
        perror("Unable to read i2c register");
        exit(-1);
    }
    return value;
}
*/


//##############################################################################################################################################################

/*
Configures both digits of the 15-segment display with no pattern (i.e. blank
the display).
*/
static void configPattern_blank(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x00 );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0x00 );
}

/*
Configures both digits of the 15-segment display with a pattern for a
full-length negative sign.
*/
static void configPattern_negSign(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x08 );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0x10 );
}

/*
Configures both digits of the 15-segment display with a pattern for '-1', where
the negative sign is shortened.
*/
static void configPattern_neg1(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x0A );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0x80 );
}

/* Configures both digits of the 15-segment display with a pattern for '0'. */
static void configPattern_0(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x86 );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0xA1 );
}

/* Configures both digits of the 15-segment display with a pattern for '1'. */
static void configPattern_1(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x02 );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0x80 );
}

/* Configures both digits of the 15-segment display with a pattern for '2'. */
static void configPattern_2(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x0E );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0x31 );
}

/* Configures both digits of the 15-segment display with a pattern for '3'. */
static void configPattern_3(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x0E );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0xB0 );
}

/* Configures both digits of the 15-segment display with a pattern for '4'. */
static void configPattern_4(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x8A );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0x90 );
}

/* Configures both digits of the 15-segment display with a pattern for '5'. */
static void configPattern_5(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x8C );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0xB0 );
}

/* Configures both digits of the 15-segment display with a pattern for '6'. */
static void configPattern_6(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x8C );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0xB1 );
}

/* Configures both digits of the 15-segment display with a pattern for '7'. */
static void configPattern_7(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x06 );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0x80 );
}

/* Configures both digits of the 15-segment display with a pattern for '8'. */
static void configPattern_8(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x8E );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0xB1 );
}

/* Configures both digits of the 15-segment display with a pattern for '9'. */
static void configPattern_9(int i2cFileDesc)
{
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_UPPER_PATTERN, 0x8E );
    write_to_I2C_reg( i2cFileDesc, GPIO_REG_ADDRESS_TO_CONFIG_LOWER_PATTERN, 0xB0 );
}

static void (*arrOfPtr2Funcs[]) (int) = {configPattern_neg1,
                                         configPattern_0,
                                         configPattern_1,
                                         configPattern_2,
                                         configPattern_3,
                                         configPattern_4,
                                         configPattern_5,
                                         configPattern_6,
                                         configPattern_7,
                                         configPattern_8,
                                         configPattern_9};

void (**ptr_configNumberPatternFuncs) (int) = (arrOfPtr2Funcs + 1); //Offset by one, so that index 0 corresponds to the function that configures the pattern for the number zero.
//OR: void (**ptr_configNumberPatternFuncs) (int) = &(arrOfPtr2Funcs[1]);


//##############################################################################################################################################################

static _Bool exitDisplayLoop = false;
static int valueToDisplay = 0;
static pthread_mutex_t setDisplayValueMutex = PTHREAD_MUTEX_INITIALIZER;

/* This function will be run inside a thread. */
static inline void* display(void* i2cFileDesc)
{
    int i2cFileDescCasted = *((int*)i2cFileDesc);

    long seconds = 0;
    long nanoseconds = 6000000; //5000000=0.005s
    struct timespec timeInterval = {seconds, nanoseconds};

    int currentlyDisplayedValue = 0;
    int leftDigitNum = 0;
    int rightDigitNum = 0;

    while( !exitDisplayLoop )
    {
        pthread_mutex_lock(&setDisplayValueMutex);
        {
                currentlyDisplayedValue = valueToDisplay;
        }
        pthread_mutex_unlock(&setDisplayValueMutex);

        if( currentlyDisplayedValue > 9 && currentlyDisplayedValue < 100 ) //Range: (9, 100)
        {
            leftDigitNum = currentlyDisplayedValue / 10;
            rightDigitNum = currentlyDisplayedValue % 10;

            turnOffLeftDigit();
            turnOffRightDigit();

            while( currentlyDisplayedValue == valueToDisplay )
            {
                (ptr_configNumberPatternFuncs[leftDigitNum]) (i2cFileDescCasted);
                turnOnLeftDigit();
                nanosleep(&timeInterval, (struct timespec *) NULL);
                turnOffLeftDigit();

                (ptr_configNumberPatternFuncs[rightDigitNum]) (i2cFileDescCasted);
                turnOnRightDigit();
                nanosleep(&timeInterval, (struct timespec *) NULL);
                turnOffRightDigit();
            }
        }
        else if( currentlyDisplayedValue >= 0 && currentlyDisplayedValue <= 9 ) //Range: [0, 9]
        {
            turnOffLeftDigit();
            turnOffRightDigit();

            (ptr_configNumberPatternFuncs[currentlyDisplayedValue]) (i2cFileDescCasted);
            turnOnRightDigit();
            while( currentlyDisplayedValue == valueToDisplay )
            {
                nanosleep(&timeInterval, (struct timespec *) NULL);
            }
        }
        else if( currentlyDisplayedValue >= -9 && currentlyDisplayedValue <= -1 ) //Range: [-9, -1]
        {
            rightDigitNum = (currentlyDisplayedValue * -1) % 10;

            turnOffLeftDigit();
            turnOffRightDigit();

            while( currentlyDisplayedValue == valueToDisplay )
            {
                configPattern_negSign(i2cFileDescCasted);
                turnOnLeftDigit();
                nanosleep(&timeInterval, (struct timespec *) NULL);
                turnOffLeftDigit();

                (ptr_configNumberPatternFuncs[rightDigitNum]) (i2cFileDescCasted);
                turnOnRightDigit();
                nanosleep(&timeInterval, (struct timespec *) NULL);
                turnOffRightDigit();
            }
        }
        else if( currentlyDisplayedValue >= -19 && currentlyDisplayedValue <= -10 ) //Range: [-19, -10]
        {
            rightDigitNum = (currentlyDisplayedValue * -1) % 10;

            turnOffLeftDigit();
            turnOffRightDigit();

            while( currentlyDisplayedValue == valueToDisplay )
            {
                configPattern_neg1(i2cFileDescCasted);
                turnOnLeftDigit();
                nanosleep(&timeInterval, (struct timespec *) NULL);
                turnOffLeftDigit();

                (ptr_configNumberPatternFuncs[rightDigitNum]) (i2cFileDescCasted);
                turnOnRightDigit();
                nanosleep(&timeInterval, (struct timespec *) NULL);
                turnOffRightDigit();
            }
        }
        else if( currentlyDisplayedValue < -19 ) //Range: (-infinity, -19)
        {
            turnOffLeftDigit();
            turnOffRightDigit();

            while( currentlyDisplayedValue == valueToDisplay )
            {
                configPattern_neg1(i2cFileDescCasted);
                turnOnLeftDigit();
                nanosleep(&timeInterval, (struct timespec *) NULL);
                turnOffLeftDigit();

                configPattern_9(i2cFileDescCasted);
                turnOnRightDigit();
                nanosleep(&timeInterval, (struct timespec *) NULL);
                turnOffRightDigit();
            }
        }
        else //Range: (99, +infinity)
        {
            turnOffLeftDigit();
            turnOffRightDigit();

            (ptr_configNumberPatternFuncs[9]) (i2cFileDescCasted);
            turnOnRightDigit();
            turnOnLeftDigit();
            while( currentlyDisplayedValue == valueToDisplay )
            {
                nanosleep(&timeInterval, (struct timespec *) NULL);
            }
        }
    }
    return NULL;
}


//##############################################################################################################################################################

static pthread_t threadID;  //To hold ID of a thread.
static int i2cFileDesc;     //To hold file descriptor for an I2C bus.

void start15SegDisp(void)
{
    //Enable Linux support for I2C bus 1:
    writeToFile( SLOTS_PATHNAME, "BB-I2C1" );

    //Enable the GPIO pins that are used to control whether the left and right digits are on or off:
    writeToFile( GPIO_EXPORT_PATHNAME, LINUX_GPIO_NUMBER_FOR_LEFT_LED_DIGIT ); //left digit
    writeToFile( GPIO_EXPORT_PATHNAME, LINUX_GPIO_NUMBER_FOR_RIGHT_LED_DIGIT ); //right rigit

    //Set the GPIO pins that control the on and off states of the left and right digits, so that those GPIO pins can be written to:
    writeToFile( LINUX_GPIO_LEFT_LED_DIRECTION_PATHNAME, "out" ); //left digit
    writeToFile( LINUX_GPIO_RIGHT_LED_DIRECTION_PATHNAME, "out" ); //right rigit

    //Get a file descriptor for reading from and writing to I2C bus 1:
    i2cFileDesc = init_I2C_bus( I2C_BUS1_PATHNAME, ADDRESS_OF_GPIO_EXTENDER );

    //Configure the GPIO extender registers that are used to configure the LED patterns, so that they can be written to:
    write_to_I2C_reg(i2cFileDesc, GPIO_REG_ADDRESS_FOR_DIRECTION_OF_REG_FOR_LOWER_HALF_OF_DIGITS, 0x00);
    write_to_I2C_reg(i2cFileDesc, GPIO_REG_ADDRESS_FOR_DIRECTION_OF_REG_FOR_UPPER_HALF_OF_DIGITS, 0x00);


    configPattern_blank(i2cFileDesc);
    pthread_mutex_lock(&setDisplayValueMutex);
    {
        valueToDisplay = 0;
    }
    pthread_mutex_unlock(&setDisplayValueMutex);
    exitDisplayLoop = false;

    pthread_create( &threadID, NULL, display, (void*)(&i2cFileDesc) );

    return;
}

void setValueToDisplay(int value)
{
    pthread_mutex_lock(&setDisplayValueMutex);
    {
        valueToDisplay = value;
    }
    pthread_mutex_unlock(&setDisplayValueMutex);
}

void stop15SegDisp(void)
{
    exitDisplayLoop = true;    //to break out of outer while loop of the display() function

    pthread_mutex_lock(&setDisplayValueMutex);
    {
        valueToDisplay = IMPOSSIBLE_CELSIUS_TEMPERATURE;    //to break out of inner while loops of the display() function
    }
    pthread_mutex_unlock(&setDisplayValueMutex);

    pthread_join( threadID, NULL );

    configPattern_blank(i2cFileDesc);
    pthread_mutex_lock(&setDisplayValueMutex);
    {
        valueToDisplay = 0;
    }
    pthread_mutex_unlock(&setDisplayValueMutex);

    close(i2cFileDesc);

    return;
}
